import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App.js';
import FirebaseContext from "./context/firebaseContext";
import Firebase from './services/database.js';
import {applyMiddleware, createStore, compose} from 'redux';
import rootReducers from './reducers';
import {Provider} from "react-redux";
import thunk from "redux-thunk";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = new createStore(rootReducers, composeEnhancers(applyMiddleware(thunk)));

ReactDOM.render(
  <Provider store={store} >
    <FirebaseContext.Provider value={new Firebase()}>
      <App />
    </FirebaseContext.Provider>
  </Provider>,
    document.getElementById('root')
);
