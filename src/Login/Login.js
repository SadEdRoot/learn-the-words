import React from 'react';
import {Form, Input, Button, Modal} from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css';
import FirebaseContext from "../context/firebaseContext";

class Login extends  React.PureComponent {

  onFinish = ({email, password}) => {
    const { signWithEmail } = this.context;

    signWithEmail(email, password)
      .then(res => {
        console.log('Received values of form: ', res);
        localStorage.setItem('user', JSON.stringify(res.user.uid));
      })
      .then(() => {
        this.props.onCreate();
      })
  };

  render() {
    return (
      <Modal
      title="Login"
      visible={this.props.visible}
      onCancel={this.props.onCancel}
      footer={null}
    >
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{
          remember: true,
        }}
        onFinish={this.onFinish}
      >
        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              message: 'Please input your email!',
            },
          ]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your Password!',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item>
          <a className="login-form-forgot" href="localhost">
            Forgot password
          </a>
          Or <a href="localhost">register now!</a>
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Log in
          </Button>

        </Form.Item>
      </Form>
    </Modal>
  );
  }
}

Login.contextType = FirebaseContext;

export default Login;
