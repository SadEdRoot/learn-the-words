import React from 'react';
import './App.css';
import HeaderBlock from './HeaderBlock/HeaderBlock.js';
import Menu from './Menu/Menu.js';
import FooterBlock from './FooterBlock/FooterBlock';
import Game from './Game/Game.js'
import Login from './Login/Login';
import {Spin} from 'antd';
import FirebaseContext from './context/firebaseContext';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import {PrivateRoute} from './utils/privateRoutes.js';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {addUserActon} from "./actions/usersAction";

class App extends React.Component {

  state = {
    loginFormVisible: false
  }

  componentDidMount() {
    const {auth, setUserUid} = this.context;
    const {addUser} = this.props;
    auth.onAuthStateChanged(user => {
      if (user) {
        setUserUid(user.uid);
        localStorage.setItem('user', JSON.stringify(user.uid));
        addUser(user);
      } else {
        setUserUid(null);
        localStorage.removeItem('user');

      }
    })
  }

  showModal = () => {
    this.setState({
      loginFormVisible: true,
    });
  };

  handleCancel = e => {
    this.setState({
      loginFormVisible: false,
    });
  };

  render() {
    const {userUid} = this.props;
    if (!userUid) {
      return (
        <div className="loader__wrap">
          <Spin size="large" />
        </div>
      )
    }

    console.log(this.props);
    return (
      <BrowserRouter>
        <div className="App">
          <Menu showModal={this.showModal}/>
          <Switch>
            <Route path="/" exact={true}>
              <HeaderBlock user={userUid} showModal={this.showModal} />
            </Route>
            <PrivateRoute path="/game" component={(props) => <Game {...props} user={userUid} />}>
            </PrivateRoute>
            <Route path="/about" render={() => <h1>About</h1>} />
            <Redirect to="/" />
          </Switch>

          <FooterBlock/>
          <Login
            visible={this.state.loginFormVisible}
            onCancel={this.handleCancel}
            onCreate={this.handleCancel}
          />
        </div>
      </BrowserRouter>

    );
  }

}

App.contextType = FirebaseContext;
const mapStateToProps = (state) => {
  return {
    userUid: state.user.userUid,
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    addUser: addUserActon,
  }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
