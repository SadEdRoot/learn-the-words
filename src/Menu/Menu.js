import React from 'react';
import './style.scss';
import logo from './logo.png';
import {Link} from "react-router-dom";
import {Button} from 'antd';

class Menu extends React.PureComponent {


  render() {
    return (
      <div className='container'>
        <nav className='main-menu'>
          <img className='logo' alt='logo' src={logo}/>
          <ul>
            <li><Link to="/">Home</Link></li>
            <li><Link to="/about">About</Link></li>
            <li><a href='https://reactmarathon.com/'>Марафон</a></li>
            <li><Button onClick={this.props.showModal}>Login</Button></li>
          </ul>
        </nav>
      </div>
    );
  }
}

export default Menu;
