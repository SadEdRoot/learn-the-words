import React, {useState} from 'react';
import s from './Card.module.scss';
import cl from 'classnames';
import {CheckSquareOutlined, DeleteOutlined} from '@ant-design/icons';

const Card = (props) => {

  const [done, toggleDone] = useState(false);
  const [isRemembered, toggleRemembered] = useState(false);

  const {eng, rus} = props;

  return (
    <div className={s.root}>
      <div
          className={ cl(s.card, {
              [s.done]: done,
              [s.isRemembered]: isRemembered,
          }) }
          onClick={() => {toggleDone(!done);}}
      >
          <div className={s.cardInner}>
              <div className={s.cardFront}>
                  {eng}
              </div>
              <div className={s.cardBack}>
                  {rus}
              </div>
          </div>
      </div>
      <div className={s.icons}>
          <CheckSquareOutlined onClick={() => {
            toggleDone(!isRemembered);
            toggleRemembered(!isRemembered);
          }}/>
      </div>
      <div className={cl(s.icons, s.deleted)}>
          <DeleteOutlined onClick={() => {props.onDelete()}}/>
      </div>
    </div>
  );
}

export default Card;
