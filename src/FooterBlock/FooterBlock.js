import React from 'react';
import s from './style.module.scss';
import {FacebookOutlined} from '@ant-design/icons';
import {ReactComponent as TelegramLogoSvg} from './telegram_logo.svg'
import {Link} from "react-router-dom";


const FooterBlock = () => {
  return (
    <footer className={s.container}>
      <nav className={s.mainMenu}>
        <ul >
          <li><Link to="/">Главная</Link></li>
          <li><Link to="/about">About</Link></li>
          <li><a href='https://reactmarathon.com/'>Марафон</a></li>
        </ul>
        <ul>
          <li><a href="https://facebook.com/"><FacebookOutlined style={{fontSize: '2em',}}/></a></li>
          <li>
            <a href="https://t.me/Sadroot"><span className='anticon' style={{fontSize: '2em',}}>
              <TelegramLogoSvg style={{display: 'inline-block', width: '1em', height: '1em',}}/>
            </span></a>
          </li>
          <li></li>
        </ul>
      </nav>
      <p className={s.copyright}>&#169; 2020 Copyright</p>
    </footer>

  )
}

export default FooterBlock;
