import React from 'react';
import './style.css';
import {Button} from 'antd';
import 'antd/dist/antd.css';
import { useHistory } from 'react-router-dom';

function HeaderBlock(props) {

  const history = useHistory();
  console.log(props);
  return (
    <>
      <div className="cover">
        <div className="wrap">
          <h1 className="header">Учите слова онлайн</h1>
          <p className="descr">Воспользуйтесь карточками для запоминания и пополнения активныйх словарных запасов</p>
          {props.user
            ? <Button size={"large"} type="primary" onClick={() => history.push('/game')}>Начать</Button>
            : <Button size={"large"} type="primary" onClick={props.showModal}>Войти</Button>
          }
        </div>
      </div>
    </>
  )
}

export default HeaderBlock;
