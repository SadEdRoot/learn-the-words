import React from 'react';
import Card from '../Card/index.js';
import {connect} from "react-redux";
import s from './Game.module.scss';
import getTranslate from '../services/dictionary.js'
import {Input} from 'antd';
import 'antd/dist/antd.css';
import FirebaseContext from "../context/firebaseContext";
import {bindActionCreators} from "redux";
import {cardListResolveAction, fetchCardList} from "../actions/cardListAction";

const { Search } = Input;

class Game extends React.Component {
  state = {
    isBusy: false,
    value: '',
  }

  inputRef = React.createRef();

  componentDidMount() {
    const { getUserCardsRef } = this.context;
    this.inputRef.current.focus();

    const { fetchCardList } = this.props;
    fetchCardList(getUserCardsRef)
  }

  setNewWord = async () => {
    const getWord = await getTranslate(this.state.value);
    const {words, fetchCardListResolve} = this.props;

    const newArr = [...words, {
      id: +new Date(),
      rus: getWord.translate,
      eng: getWord.text,
    }];
    const { getUserCardsRef } = this.context;
    getUserCardsRef().set(newArr);
    fetchCardListResolve(newArr);

    this.setState((state) => ({
      isBusy: false,
      value: '',
    }));

  }

  handleDeleteItem = (id) => {
    const {fetchCardListResolve} = this.props;
    const newArr = this.props.words.filter((el)=>{return el.id !== id});
    const { getUserCardsRef } = this.context;
    getUserCardsRef().set(newArr);
    fetchCardListResolve(newArr);
  }

  handleChangeInput = ({target}) => {
    this.setState({
      value: target.value,
    });
  }

  handleSubmitForm = async () => {
    this.setState({
      isBusy: true,
    }, this.setNewWord);
  }

  render() {
    const {isBusy, value} = this.state;
    const {words} = this.props;
    console.dir(this.props);
    return (
      <div className={s.cover}>
        <div className={s.form}>
          <Search
            placeholder="Введите слово для перевода"
            enterButton="Добавить в словарь"
            size="large"
            value = {value}
            onChange={this.handleChangeInput}
            onSearch={this.handleSubmitForm}
            ref={this.inputRef}
            loading={isBusy}
          />
        </div>

        <p>Нажмите на карточку, чтобы увидеть перевод</p>
        <div className={s.cardContainer}>
          {words.map(({eng , rus, id}) => (<Card eng={eng} rus={rus} key={id} onDelete={() => this.handleDeleteItem(id)}/>))}
        </div>

      </div>
    );
  }

}

Game.contextType = FirebaseContext;

const mapStateToProps = (state) => {
  return {
    isBusy: state.cardList.isBusy,
    words: state.cardList.payload || [],
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchCardList: fetchCardList,
    fetchCardListResolve: cardListResolveAction,
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Game)
