import { combineReducers} from "redux";
import usersReducer from "./usersReducer";
import counterReducer from "./counterReducer";
import cardListReducer from "./cardListReducer";

export default combineReducers({
  user: usersReducer,
  counter: counterReducer,
  cardList: cardListReducer,
});
